import java.util.concurrent.Semaphore;
/*
    La cena de los filósofos. Proyecto donde 5 filósofos cenan y piensan. El problema es que no hay palillos para todos
    y tienen que compartirlos.
 */
public class Main {

    final static int[][] palillos = {
            {0,4}, // filósofo 1.
            {1,0}, // filósofo 2.
            {2,1}, // filósofo 3.
            {3,2}, // filósofo 4.
            {4,3}, // filósofo 5.
    };
    // Array semaforo con los 5 filósofos.
    final static Semaphore[] semaforoPalillos = new Semaphore[5];
    public static void main(String[] args) {

        for(int i=0; i<5; i++) {
            semaforoPalillos[i] = new Semaphore(i);
        }
        // Se crean los objetos filósofos con el id, el semáforo y los palillos.
        for(int numFilosofo = 0; numFilosofo<5; numFilosofo++) {
            new filosofo(numFilosofo, semaforoPalillos, palillos).start();
        }
    }
}