import java.util.Random;
import java.util.concurrent.Semaphore;

public class filosofo extends Thread{

    private final int id;
    private final Semaphore[] semaforoPalillos;
    private final int [][] palillos;
    private final int palilloIzquierdo;
    private final int palilloDerecho;
    private final Random tiempo = new Random();
    // Constructor con el id del filósofo, el semáforo y el array de los palillos.
    public filosofo(int id, Semaphore[] semaforoPalillos, int[][] palillos){
        this.id = id;
        this.semaforoPalillos = semaforoPalillos;
        this.palillos = palillos;
        this.palilloIzquierdo = palillos[id][0];
        this.palilloDerecho = palillos[id][1];
    }
    // El método pensar, muestra un mensaje y pone a pensar al filósofo un tiempo aleatorio.
    protected void pensar(){
        System.out.println("El filosofo "+id+" está pensando.");
        try{
            filosofo.sleep(tiempo.nextInt(3000)+1000);
        }catch (InterruptedException ie){
            System.err.println("Error: "+ie.toString());
        }
    }
    /*
        El método comer:
        Primero intenta coger el palillo izquierdo, si lo consigue intenta coger el derecho, si lo consigue come
        durante un tiempo aleatorio. Después libera los palillos.
        Si no puede coger algún palillo pasa al else y muestra el mensaje de que no puede comer.
     */
    protected void comer(){
        if(semaforoPalillos[palilloIzquierdo].tryAcquire()){
            if(semaforoPalillos[palilloDerecho].tryAcquire()){
                System.out.println("El filosofo "+id+" está comiendo.");
                try{
                    sleep(tiempo.nextInt(5000)+1000);
                }catch (InterruptedException ie){
                    System.err.println("Error: "+ie.toString());
                }
                semaforoPalillos[palilloDerecho].release();
            }
            semaforoPalillos[palilloIzquierdo].release();
        }else {
            System.out.println("El filosofo "+id+" no puede comer.");
        }
    }

    @Override
    public void run(){
        while (true){
            pensar();
            comer();
        }
    }
}
